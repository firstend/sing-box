package outbound

import (
	"context"
	"fmt"
	"net"
	"net/netip"
	"net/rpc"
	"os"
	"sync"
	"time"

	"github.com/sagernet/sing-box/adapter"
	C "github.com/sagernet/sing-box/constant"
	"github.com/sagernet/sing/common"
	"github.com/sagernet/sing/common/buf"
	"github.com/sagernet/sing/common/bufio"
	"github.com/sagernet/sing/common/canceler"
	E "github.com/sagernet/sing/common/exceptions"
	N "github.com/sagernet/sing/common/network"
)

func NewMyTraficCouner(inboundTag, user string) *MyTraficCounter {

	tc := &MyTraficCounter{tag: inboundTag, sum: 0, user: user}
	return tc
}

type MyTraficCounter struct {
	sync.Mutex
	tag  string
	sum  int64
	user string
}

func (c *MyTraficCounter) Add(n int64) {
	c.Lock()
	defer c.Unlock()
	c.sum += n
}

type Report struct {
	InboundTag string `json:"inbound_tag"`
	User       string `json:"user"`
	Trafic     int64  `json:"trafic"`
}

func (c *MyTraficCounter) SendReport() {
	c.Lock()
	s := c.sum
	c.Unlock()
	client, err := rpc.DialHTTP("tcp", "127.0.0.1:3001")
	if err != nil {
		return
	}
	args := Report{
		InboundTag: c.tag,
		User:       c.user, Trafic: s,
	}
	var reply bool
	client.Call("API.NewInboundTraficReport", args, &reply)
}
func (c *MyTraficCounter) ReportSum() string {
	c.Lock()
	defer c.Unlock()
	s := c.sum
	return fmt.Sprintf("%s used %d bytes", c.user, s)
}
func MNewConnection(ctx context.Context, this N.Dialer, conn net.Conn, metadata adapter.InboundContext) error {
	ctx = adapter.WithContext(ctx, &metadata)
	var outConn net.Conn
	var err error
	if len(metadata.DestinationAddresses) > 0 {
		outConn, err = N.DialSerial(ctx, this, N.NetworkTCP, metadata.Destination, metadata.DestinationAddresses)
	} else {
		outConn, err = this.DialContext(ctx, N.NetworkTCP, metadata.Destination)
	}
	if err != nil {
		return N.HandshakeFailure(conn, err)
	}
	return MCopyEarlyConn(ctx, conn, outConn, metadata)
}

func MNewPacketConnection(ctx context.Context, this N.Dialer, conn N.PacketConn, metadata adapter.InboundContext) error {
	tc := NewMyTraficCouner(metadata.Inbound, metadata.User)
	ctx = adapter.WithContext(ctx, &metadata)
	ctx = adapter.WithContext(ctx, &metadata)
	var outConn net.PacketConn
	var destinationAddress netip.Addr
	var err error
	if len(metadata.DestinationAddresses) > 0 {
		outConn, destinationAddress, err = N.ListenSerial(ctx, this, metadata.Destination, metadata.DestinationAddresses)
	} else {
		outConn, err = this.ListenPacket(ctx, metadata.Destination)
	}
	if err != nil {
		return N.HandshakeFailure(conn, err)
	}
	if destinationAddress.IsValid() {
		if natConn, loaded := common.Cast[bufio.NATPacketConn](conn); loaded {
			natConn.UpdateDestination(destinationAddress)
		}
	}
	switch metadata.Protocol {
	case C.ProtocolSTUN:
		ctx, conn = canceler.NewPacketConn(ctx, conn, C.STUNTimeout)
	case C.ProtocolQUIC:
		ctx, conn = canceler.NewPacketConn(ctx, conn, C.QUICTimeout)
	case C.ProtocolDNS:
		ctx, conn = canceler.NewPacketConn(ctx, conn, C.DNSTimeout)
	}
	err = bufio.MCopyPacketConn(ctx, conn, bufio.NewPacketConn(outConn), tc)
	go tc.SendReport()
	return err
}

func MCopyEarlyConn(ctx context.Context, conn net.Conn, serverConn net.Conn, metadata adapter.InboundContext) error {
	tc := NewMyTraficCouner(metadata.Inbound, metadata.User)
	if cachedReader, isCached := conn.(N.CachedReader); isCached {
		payload := cachedReader.ReadCached()
		if payload != nil && !payload.IsEmpty() {
			_, err := serverConn.Write(payload.Bytes())
			if err != nil {
				return err
			}
			err = bufio.MCopyConn(ctx, conn, serverConn, tc)
			go tc.SendReport()
			return err
		}
	}
	if earlyConn, isEarlyConn := common.Cast[N.EarlyConn](serverConn); isEarlyConn && earlyConn.NeedHandshake() {
		payload := buf.NewPacket()
		err := conn.SetReadDeadline(time.Now().Add(C.ReadPayloadTimeout))
		if err != os.ErrInvalid {
			if err != nil {
				return err
			}
			_, err = payload.ReadOnceFrom(conn)
			if err != nil && !E.IsTimeout(err) {
				return E.Cause(err, "read payload")
			}
			err = conn.SetReadDeadline(time.Time{})
			if err != nil {
				payload.Release()
				return err
			}
		}
		_, err = serverConn.Write(payload.Bytes())
		if err != nil {
			return N.HandshakeFailure(conn, err)
		}
		payload.Release()
	}
	err := bufio.MCopyConn(ctx, conn, serverConn, tc)
	go tc.SendReport()
	return err
}
