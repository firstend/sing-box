package box

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
)

type RPCBox struct {
	box    *Box
	apiKey string
}

func NewRPCBox(box *Box) *RPCBox {
	k := os.Getenv("SING_STATS_API_KEY")
	if k == "" {
		log.Fatal("SING_STATS_API_KEY is required")
	}
	return &RPCBox{
		box:    box,
		apiKey: k,
	}
}

type InboundUsers struct {
	Tag    string `json:"tag"`
	Users  []byte `json:"users"`
	APIKey string `json:"api_key"`
}

func (r *RPCBox) Serve() error {
	l, err := net.Listen("tcp", "127.0.0.1:3006")
	if err != nil {
		return err
	}
	s := rpc.NewServer()
	s.Register(r)
	s.HandleHTTP(rpc.DefaultRPCPath, rpc.DefaultDebugPath)
	go http.Serve(l, s)
	return nil
}
func (r *RPCBox) SetInboundUsers(iu InboundUsers, done *bool) error {
	if r.apiKey != iu.APIKey {
		return fmt.Errorf("Unathorized api key")
	}
	err := r.box.SetInboundUsers(iu.Tag, iu.Users)
	if err != nil {
		return err
	}
	*done = true
	return nil
}
