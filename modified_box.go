package box

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"math/big"
	"time"

	"github.com/gofrs/uuid/v5"
	C "github.com/sagernet/sing-box/constant"
	"github.com/sagernet/sing-box/inbound"
	"github.com/sagernet/sing-box/option"
	// "github.com/sagernet/sing-box/option"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		nn, err := rand.Int(rand.Reader, big.NewInt(int64(len(letterRunes))))
		if err != nil {
			return time.Now().String()
		}
		b[i] = letterRunes[nn.Int64()]
	}
	return string(b)
}
func (s *Box) SetInboundUsers(inboundTag string, users []byte) error {
	// users := []byte("[{\"username\":\"admin\",\"password\":\"aa\"}]")
	for i := range s.mOriginalOptions.Options.Inbounds {
		optInb := s.mOriginalOptions.Options.Inbounds[i]
		if optInb.Tag == inboundTag {
			for j := range s.inbounds {
				inb := s.inbounds[j]
				if inb.Tag() == inboundTag {
					if err := inb.Close(); err != nil {
						return err
					}
					switch optInb.Type {
					// case C.TypeSocks:
					// 	if err := json.Unmarshal(users, &optInb.SocksOptions.Users); err != nil {
					// 		return err
					// 	}
					case C.TypeShadowTLS:
						optInb.ShadowTLSOptions.Users = []option.ShadowTLSUser{}
						if err := json.Unmarshal(users, &optInb.ShadowTLSOptions.Users); err != nil {
							return err
						}
						//This is because sing box would fail in ShadowTLS V3 empty array users
						if len(optInb.ShadowTLSOptions.Users) == 0 {
							optInb.ShadowTLSOptions.Users = []option.ShadowTLSUser{
								{Name: randStringRunes(10), Password: randStringRunes(20)},
							}
						}
					case C.TypeShadowsocks:
						optInb.ShadowsocksOptions.Users = []option.ShadowsocksUser{}
						if err := json.Unmarshal(users, &optInb.ShadowsocksOptions.Users); err != nil {
							return err
						}
					case C.TypeVLESS:
						if err := json.Unmarshal(users, &optInb.VLESSOptions.Users); err != nil {
							return err
						}
						if len(optInb.VLESSOptions.Users) == 0 {
							uid, err := uuid.NewV4()
							if err != nil {
								return fmt.Errorf("failed to generate uuid : %s", err)
							}
							optInb.VLESSOptions.Users = []option.VLESSUser{
								{Name: randStringRunes(10), UUID: uid.String()},
							}
						}
					case C.TypeVMess:

						if err := json.Unmarshal(users, &optInb.VMessOptions.Users); err != nil {
							return err
						}
						if len(optInb.VMessOptions.Users) == 0 {
							uid, err := uuid.NewV4()
							if err != nil {
								return fmt.Errorf("failed to generate uuid : %s", err)
							}
							optInb.VMessOptions.Users = []option.VMessUser{
								{Name: randStringRunes(10), UUID: uid.String()},
							}
						}
					default:
						return fmt.Errorf("Inboud type %s is not supoorted for this change", optInb.Type)
					}
					s.mOriginalOptions.Options.Inbounds[i] = optInb
					newInb, err := inbound.New(s.mOriginalOptions.Context, s.router, s.logger, optInb, s.mOriginalOptions.PlatformInterface)
					if err != nil {
						return err
					}
					s.inbounds[j] = newInb
					if err := s.inbounds[j].Start(); err != nil {
						return err
					}
				}
			}

		}
	}
	return nil
}
